Yii2 Codeception Model Tester
===========================

This is the model tester module for yii2 codeception.

The main repository is <https://gitlab.upc.edu/caminstic/yii2-codeception>.
Please submit issue reports and pull requests to <https://packagist.org/packages/caminstech/yii2-codeception>.

Installation
------------

The preferred way to install Yii2 Codeception Model Tester is through [composer](http://getcomposer.org/download/).
Add to composer json file
```json
"caminstech/yii2-codeception": "*",
```

Once package is installed add this lines to functional.suite.yml
```yml
modules:
    enabled:
        - \caminstech\codeception\src\helpers\ModelTesterHelper
        - \caminstech\codeception\src\tester\ModelTester
```

Usage
-----

Then you have to create a helper for each model to test, create a class that extends *\caminstech\codeception\src\tester\BaseModel* if you want the default configuration or a class that implements *\caminstech\codeception\src\tester\iModel*. If you are not familiarized yet just extend BaseModel.  
First you have to define one method:
```php
public function getModelClassName()
{
    return MyModel::className();
}
```

After we need to define the displayed name of the model (by default is first upper and final s, mymodel to Mymodels, so if it fits dont define this method):
```php
public function getDisplayName();
{
    return 'My Model';
}
```

If your model display information in index or model information page define this methods, by default they are void:
```php
public function getIndexInformation($model)
{
    return [
      $model->attribute1,
      $model->attribute2,
      $model->attribute3,
    ];
}

public function getModelInformationPageInformation($model)
{
    return [
      'NameDisplayedAttribute1' => $model->attribute1,
      'NameDisplayedAttribute2' => $model->attribute2,
      'NameDisplayedAttribute3' => $model->attribute3,
    ];
}
```

Finally if it has a form to create or update model define this. Note that this function should be used with codeception yii2 functions checkOption, uncheckOption, fillField and click:
```php
public function fillFields($attributes)
{
    $this->I->fillField('firstAttribute', $attributes['first']);
    $this->I->checkOption('#CheckBox');
    $this->I->click('Finish');
}
```

Now we need to create a test. Create a standart codeception test like this:
```php
namespace app\tests\functional;

use MyModelHelper;

use app\tests\fixtures\UserFixture;
use app\tests\fixtures\MyModelFixture;

class MyModelCest
{
    private $user;
    private $myModel;
    private $myModelHelper;

    public function _before(\FunctionalTester $I)
    {
        $I->haveFixtures([
            'users' => UserFixture::className(),
            'myModels' => MyModelFixture::className(),
        ]);

        $this->user = $I->grabFixture('users', 'user');
        $this->myModel = $I->grabFixture('myModels', 'myModel');
        $this->myModelHelper = new MyModelHelper($I);
    }

    public function testMyModel(\FunctionalTester $I)
    {
        $I->wantTo('Test something of my model');

        $I->checkViewButtonInIndex($this->user,$this->myModelHelper,$this->myModel);
    }
}
```
All new $I methods are explained in ModelTester.php and there are examples of use in examples folder.

License
-------

Copyright (C) 2015-2016 Universitat Politècnica de Catalunya - UPC BarcelonaTech - www.upc.edu

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
ERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
