<?php

namespace caminstech\codeception\src\tester;

class ModelTester extends \Codeception\Module
{
    private $I;
    private $modelTesterHelper;

    public function _initialize()
    {
        $this->I = $this->getModule('Yii2');
        $this->modelTesterHelper = $this->getModule('\caminstech\codeception\src\helpers\ModelTesterHelper');
    }

    /**
     * Check that in a page there's a link to the index page of the model and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * By default
     * $I->checkIndexButtonOnPage($user,$myModelHelper);
     * With page
     * $I->checkIndexButtonOnPage($user,$myModelHelper,'/Page/Where/Button/Is');
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param String page to assert index button by default root
     *
     */
    public function checkIndexButtonOnPage($user,$helper,$page = '/')
    {
        $this->modelTesterHelper->seeLinksOnPage($user, $page, $helper->getIndexLink());
        $this->modelTesterHelper->checkPageOK($user, [$helper->getIndexPage()]);
    }

    /**
     * Check that in a page (by default root) there's no link to the index page of the model and user can't access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * By default
     * $I->checkIndexButtonOnPageNOTFOUND($user,$myModelHelper);
     * With page
     * $I->checkIndexButtonOnPageNOTFOUND($user,$myModelHelper,'/Page/Where/Button/Isnt');
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param String page to not assert index button by default root
     *
     */
    public function checkIndexButtonOnPageNOTFOUND($user,$helper, $page = '/')
    {
        $this->modelTesterHelper->dontSeeOnPage($user, $page, [$helper->getDisplayName()]);
        $this->modelTesterHelper->checkPageNOTFOUND($user, [$helper->getIndexPage()]);
    }

    /**
     * Check that in a row of a table in Index page there's the view button and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkViewButtonInIndex($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkViewButtonInIndex($user,$helper,$model)
    {
        $this->checkButtonsInARow($user,$helper->getIndexPage(),$helper,$model,['view']);
    }

    /**
     * Check that in a row of a table in Index page there's the update button and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkUpdateButtonInIndex($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkUpdateButtonInIndex($user,$helper,$model)
    {
        $this->checkButtonsInARow($user,$helper->getIndexPage(),$helper,$model,['update']);
    }

    /**
     * Check that in a row of a table in Index page there's the delete button and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkDeleteButtonInIndex($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assertd
     *
     */
    public function checkDeleteButtonInIndex($user,$helper,$model)
    {
        $this->checkButtonsInARow($user,$helper->getIndexPage(),$helper,$model,['delete']);
    }

    /**
     * Check that in a row of a table in Index page there's the view, update and delete buttons and user can access view and update
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkCommonButtonsInIndex($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkCommonButtonsInIndex($user,$helper,$model)
    {
        $this->checkButtonsInARow($user,$helper->getIndexPage(),$helper,$model,['view','update','delete']);
    }

    /**
     * Check that in a row of a table in page there's the actions buttons and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $page = 'Page/To/Assert/Buttons/In/A/Row';
     * $actions = ['action1','action2','action3'];
     * $I->checkButtonsInARow($user,$page,$myModelHelper,$myModel,$actions);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param String page to assert buttons in a row
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     * @param Array list of the actions of the buttons
     *
     */
    public function checkButtonsInARow($user,$page,$helper,$model,$actions)
    {
        $this->I->amLoggedInAs($user);
        foreach ($actions as $action) {
          $this->I->amOnPage($page);
          if($action == 'view') $this->I->seeElement('tr[data-key='.$model->id.'] > td > a[href="/'.$helper->getModelId().'/'.$model->id.'"]');
          else $this->I->seeElement('tr[data-key='.$model->id.'] > td > a[href="/'.$helper->getModelId().'/'.$action.'/'.$model->id.'"]');
          if($action != 'delete') $this->modelTesterHelper->checkPageOK($user,['/'.$helper->getModelId().'/'.$action.'/'.$model->id]);
        }
    }

    /**
     * Check that in model information page there's a link to the update page and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkUpdateButtonInModelPage($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkUpdateButtonInModelPage($user,$helper,$model)
    {
        $this->checkButtonsOnPage($user,'/'.$helper->getModelId().'/'.$model->id,$helper,$model,['Update' => 'update']);
    }

    /**
     * Check that in model information page there's a link to the delete page
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkDeleteButtonInModelPage($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkDeleteButtonInModelPage($user,$helper,$model)
    {
        $this->checkButtonsOnPage($user,'/'.$helper->getModelId().'/'.$model->id,$helper,$model,['Delete' => 'delete']);
    }

    /**
     * Check that in index page there's a link to the create page and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkCreateButtonInIndex($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkCreateButtonInIndex($user,$helper)
    {
        $this->checkButtonsOnPage($user,$helper->getIndexPage(),$helper,NULL,['Create' => 'create']);
    }

    /**
     * Check that in model information page there's a link to the update and delete page and user can access to update
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkCommonButtonsInModelPage($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkCommonButtonsInModelPage($user,$helper,$model)
    {
        $this->checkButtonsOnPage($user,'/'.$helper->getModelId().'/'.$model->id,$helper,$model,['Update' => 'update', 'Delete' => 'delete']);
    }

    /**
     * Check that in page there's a link to the model action and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $page = 'Page/To/Assert/Buttons/On/Page';
     * $buttons = [
     *    'nameDisplayed1' => 'buttonAction1',
     *    'nameDisplayed2' => 'buttonAction2',
     *    'nameDisplayed3' => 'buttonAction3'
     *   ];
     * $I->checkButtonsOnPage($user,$page,$myModelHelper,$myModel,$buttons);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param String page to assert buttons in a row
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     * @param Array with the name of the button as key and the action of the button as value
     *
     */
    public function checkButtonsOnPage($user,$page,$helper,$model,$buttons)
    {
        $this->I->amLoggedInAs($user);
        foreach ($buttons as $buttonName => $buttonAction) {
            $this->I->amOnPage($page);
            if($buttonAction == 'create')
            {
              $this->I->seeLink($buttonName, '/'.$helper->getModelId().'/'.$buttonAction);
              $this->modelTesterHelper->checkPageOK($user,['/'.$helper->getModelId().'/'.$buttonAction]);
            }
            else
            {
              $this->I->seeLink($buttonName, '/'.$helper->getModelId().'/'.$buttonAction.'/'.$model->id);
              if($buttonAction != 'delete') $this->modelTesterHelper->checkPageOK($user,['/'.$helper->getModelId().'/'.$buttonAction.'/'.$model->id]);
            }
        }
    }

    /**
     * Check that in index there's the model row with view, update and delete buttons and in model information page there's update and delete buttons and user can access
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkCommonButtons($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkCommonButtons($user,$helper,$model)
    {
        $this->checkCommonButtonsInIndex($user,$helper,$model);
        $this->checkCommonButtonsInModelPage($user,$helper,$model);
        $this->checkCreateButtonInIndex($user,$helper);
    }

    /**
     * Check that in index page correct information is displayed in model row. Also check the information of the table in model information page.
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkModelInformation($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkModelInformation($user,$helper,$model)
    {
        $this->modelTesterHelper->seeRowOnPage($user,$helper->getIndexPage(),$model->id,$helper->getIndexInformation($model));
        $this->modelTesterHelper->checkTableInformation($user,'/'.$helper->getModelId().'/'.$model->id,$helper->getModelInformationPageInformation($model));
    }

    /**
     * Check that model form can be filled by user and it's created correctly in BD
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $attributes = [
     *  'attribute1' => 'newValue1',
     *  'attribute2' => 'newValue2',
     *  'attribute3' => 'newValue3',
     * ];
     * $I->checkCreateModel($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param Array with the atributtes to fill in model form
     *
     */
    public function checkCreateModel($user,$helper,$attributes)
    {
        $this->fillAndCheckModelForm($user,'/'.$helper->getModelId().'/create',$helper,$attributes,'Create');
    }

    /**
     * Check that model form can be filled by user and it's updated correctly in BD
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $attributes = [
     *  'attribute1' => 'newValue1',
     *  'attribute2' => 'newValue2',
     *  'attribute3' => 'newValue3',
     * ];
     * $I->checkUpdateModel($user,$myModel,$myModelHelper,$attributes);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     * @param Array with the atributtes to fill in model form
     *
     */
    public function checkUpdateModel($user,$model,$helper,$attributes)
    {
        $this->fillAndCheckModelForm($user,'/'.$helper->getModelId().'/update/'.$model->id,$helper,$attributes,'Update');
    }

    /**
     * Check that AjaxPostRequest can be sent to a model by user and it's deleted correctly in BD
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $myModel = $I->grabFixture('myModels', 'myModel');
     * $I->checkDeleteModel($user,$myModelHelper,$myModel);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param \yii\db\ActiveRecord model to assert
     *
     */
    public function checkDeleteModel($user,$helper,$model)
    {
        $this->I->amLoggedInAs($user);
        $this->I->sendAjaxPostRequest('/'.$helper->getModelId().'/delete/'.$model->id, ['_csrf' => \Yii::$app->request->getCsrfToken()]);
        $this->I->dontSeeRecord($helper->getModelClassName(), $model->attributes);
    }

    /**
     * Check that page form can be filled by user and it's correctly created or updated in BD
     *
     * for example
     * ```php
     * $myModelHelper = new myModelHelper($I);
     * $user = $I->grabFixture('users', 'user1');
     * $page = '/Page/To/Fill/The/Model/Form';
     * $attributes = [
     *  'attribute1' => 'newValue1',
     *  'attribute2' => 'newValue2',
     *  'attribute3' => 'newValue3',
     * ];
     * $buttonName = 'finishedFormButtonName';
     * $I->fillAndCheckModelForm($user,$page,$myModelHelper,$attributes,$buttonName);
     * ```
     *
     * @param \yii\web\IdentityInterface user in page
     * @param String page where the form is filled
     * @param \caminstech\codeception\src\tester\iModel model helper
     * @param Array with the atributtes to fill in model form
     * @param String with the name displayed of the button that confirms the form
     *
     */
    public function fillAndCheckModelForm($user,$page,$helper,$attributes,$buttonName)
    {
        $this->I->amLoggedInAs($user);
        $this->I->amOnPage($page);
        $helper->fillFields($attributes);
        $this->I->click($buttonName);
        $this->I->seeRecord($helper->getModelClassName(),$attributes);
    }
}
