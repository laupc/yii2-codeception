<?php
namespace caminstech\codeception\src\tester;
use yii\helpers\Inflector;

abstract class BaseModel implements iModel
{
    protected $I;

    /**
     * Constructor function
     *
     * Note that this function has the $I of the codeception teser as param
     *
     * Usage
     * ```php
     * use Namespace\modelHelper;
     * private $myModelHelper;
     *
     * public function _before(\FunctionalTester $I){
     *    $this->myModelHelper = new modelHelper($I);
     * }
     * ```
     *
     * @param Object codeception functional tester
     */
    function __construct($I) {
        $this->I = $I;
    }

    /**
     * {@inheritdoc}
     * By default return the name genereted by Yii2
     */
    public function getModelId()
    {
        $parts = explode('\\', $this->getModelClassName());
        return Inflector::camel2id(array_pop($parts));
    }

    /**
     * {@inheritdoc}
     * By default return the ModelId with the first character in UpperCase and a final s
     */
    public function getDisplayName()
    {
        return ucfirst($this->getModelId()).'s';
    }

    /**
     * {@inheritdoc}
     * By default return the yii2 generated index page in PrettyUrl
     */
    public function getIndexPage()
    {
        return '/'.$this->getModelId().'/index';
    }

    /**
     * {@inheritdoc}
     * By default return an array with the defined display name as key and the defined index url as value
     */
    public function getIndexLink()
    {
        return [$this->getDisplayName() => $this->getIndexPage()];
    }

    /**
     * {@inheritdoc}
     * By default return an empty array
     */
    public function getIndexInformation($model)
    {
        return [];
    }

    /**
     * {@inheritdoc}
     * By default return an empty array
     */
    public function getModelInformationPageInformation($model)
    {
        return [];
    }

    /**
     * {@inheritdoc}
     * By default does nothing
     */
    public function fillFields($attributes){}

    /**
     * {@inheritdoc}
     * By default return this class
     */
    public function __toString()
    {
        return get_class($this);
    }
}
