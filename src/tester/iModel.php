<?php
namespace caminstech\codeception\src\tester;

interface iModel
{
    /**
     * Returns the name of the model in Yii
     *
     * for example
     * ```php
     * return 'mymodel';
     * ```
     *
     * @return String name of the model in Yii
     */
    public function getModelId();

    /**
     * Returns the name of the model displayed in the webpage
     *
     * for example
     * ```php
     * return 'My Model';
     * ```
     *
     * @return String name displayed of the model
     */
    public function getDisplayName();

    /**
     * Returns the page where this models are displayed (index)
     *
     * for example
     * ```php
     * return '/mymodel/index';
     * ```
     *
     * @return String the link to index in PrettyUrl
     */
    public function getIndexPage();

    /**
     * Returns an array with the name of the model displayed in the webpage and the page to its index
     *
     * for example
     * ```php
     * return ['mymodel'=>'/mymodel/index'];
     * ```
     *
     * @return Array with the model ID of the key and the link to index as value
     */
    public function getIndexLink();

    /**
     * Return an array with the data displayed in a row of the table on index page
     *
     * for example
     * ```php
     * return [
     *  $model->indexAttribute1,
     *  $model->indexAttribute2,
     *  $model->indexAttribute3,
     * ];
     * ```
     *
     * @param \yii\db\ActiveRecord model to assert atributes
     * @return Array with all atributes of the model displayed in index
     */
    public function getIndexInformation($model);

    /**
     * Return an array with the data displayed in the table on model information page
     *
     * for example
     * ```php
     * return [
     *  $model->informationPageAttribute1,
     *  $model->informationPageAttribute2,
     *  $model->informationPageAttribute3,
     * ];
     * ```
     *
     * @param \yii\db\ActiveRecord model to assert atributes
     * @return Array with all atributes of the model displayed in model information page
     */
    public function getModelInformationPageInformation($model);

    /**
     * Fills the fields of a model form
     *
     * Note that this function should be used with codeception yii2 functions checkOption, uncheckOption, fillField and click
     *
     * for example
     * ```php
     * $this->I->fillField('firstAttribute', $attributes['first']);
     * $this->I->checkOption('#CheckBox');
     * $this->I->click('Finish');
     * ```
     *
     * @param Array of the attributes needed to fill the model form
     * @return void
     */
    public function fillFields($attributes);

    /**
     * Have to return the model className
     *
     * This function must be like this:
     * ```php
     * use Namespace/MyModel;
     * public function getModelClassName(){
     *   return MyModel::className();
     * }
     *```
     *
     * @return String className of the model
     */
    public function getModelClassName();

    /**
     * Returns the name displayed when codeception debug, you can modify
     *
     * for example
     * ```php
     * return 'My Model';
     * ```
     *
     * @return String of the name displayed in codeception tests
     */
    public function __toString();
}
