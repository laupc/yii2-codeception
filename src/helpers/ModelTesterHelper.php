<?php
namespace caminstech\codeception\src\helpers;

class ModelTesterHelper extends \Codeception\Module
{
    private $I;

    public function _beforeSuite($settings = array()) {
        $this->I = $this->getModule('Yii2');
    }

    public function seeOnPage($user,$page,$see)
    {
        $this->OnPage($user,$page,$see,true);
    }

    public function dontSeeOnPage($user,$page,$see)
    {
        $this->OnPage($user,$page,$see,false);
    }

    private function OnPage($user,$page,$see,$switch)
    {
        $this->I->amLoggedInAs($user);
        $this->I->amOnPage($page);
        foreach ($see as $string) {
            $string = ($string == null ? '' : $string);
            $switch ? $this->I->see($string) : $this->I->dontSee($string);
        }
    }

    public function checkModelLinksInARow($user,$model,$dataKey)
    {
        $this->checkLinksInARow($user,$model,$dataKey,false);
    }

    public function checkEntityLinksInARow($user,$entity,$dataKey)
    {
        $this->checkLinksInARow($user,$entity,$dataKey,true);
    }

    private function checkLinksInARow($user,$model,$dataKey,$isentity)
    {
        $this->I->amLoggedInAs($user);
        $this->I->amOnPage('/'.$model.'/index');
        $this->I->seeElement('tr[data-key='.$dataKey.'] > td > a[href="/'.$model.'/'.$dataKey.'"]');
        if($isentity) $this->I->seeElement('tr[data-key='.$dataKey.'] > td > a[href="/'.$model.'/update/'.$dataKey.'"]');
        $this->I->seeElement('tr[data-key='.$dataKey.'] > td > a[href="/'.$model.'/delete/'.$dataKey.'"]');
    }

    public function checkTableInformation($user,$page,$information)
    {
        $this->I->amLoggedInAs($user);
        $this->I->amOnPage($page);
        foreach ($information as $name => $data) {
          $this->I->see($name, ['css' => 'tr > th']);
          $this->I->see($data, ['css' => 'tr > td']);
        }
    }

    public function seeRowOnPage($user,$page,$dataKey,$row)
    {
        $this->rowOnPage($user,$page,$dataKey,$row,true);
    }

    public function dontSeeRowOnPage($user,$page,$dataKey,$row)
    {
        $this->rowOnPage($user,$page,$dataKey,$row,false);
    }

    private function rowOnPage($user,$page,$dataKey,$row,$switch)
    {
        $this->I->amLoggedInAs($user);
        $this->I->amOnPage($page);
        foreach ($row as $string) {
           $string = ($string == null ? '' : $string);
           $switch ? $this->I->see($string,['css'=>'tr[data-key='.$dataKey.'] > td']) : $this->I->dontSee($string,['css'=>'tr[data-key='.$dataKey.'] > td']);
        }
    }

    public function seeLinksOnPage($user,$page,$see)
    {
        $this->linksOnPage($user,$page,$see,true);
    }

    public function dontSeeLinksOnPage($user,$page,$see)
    {
        $this->linksOnPage($user,$page,$see,false);
    }

    private function linksOnPage($user,$page,$see,$switch)
    {
        $this->I->amLoggedInAs($user);
        $this->I->amOnPage($page);
        foreach ($see as $string => $link) {
                $string = ($string == null ? '' : $string);
                $switch ? $this->I->seeLink($string,$link) : $this->I->dontSeeLink($string,$link);
        }
    }

    public function checkPageNOTFOUND($user,$pages)
    {
        $this->checkPage($user,$pages,\Codeception\Util\HttpCode::NOT_FOUND);
    }

    public function checkPageOK($user,$pages)
    {
        $this->checkPage($user,$pages,\Codeception\Util\HttpCode::OK);
    }

    public function checkPageFORBIDDEN($user,$pages)
    {
        $this->checkPage($user,$pages,\Codeception\Util\HttpCode::FORBIDDEN);
    }

    private function checkPage($user,$pages,$code)
    {
        $this->I->amLoggedInAs($user);
        foreach($pages as $page)
        {
            $this->I->amOnPage($page);
            $this->I->seeResponseCodeIs($code);
        }
    }
}
