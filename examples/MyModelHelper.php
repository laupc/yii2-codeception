<?php
namespace MyModel;

use caminstech\codeception\src\tester\BaseModel;
use models\MyModel;

class MyModelHelper extends BaseModel
{
    public function getModelClassName()
    {
        return MyModel::className();
    }

    public function getDisplayName()
    {
        return 'My Models';
    }

    public function getIndexInformation($model)
    {
        return [
            $model->name,
            $model->number,
            $model->date,
        ];
    }
    public function getModelInformationPageInformation($model)
    {
        return[
            'Name' => $model->name,
            'Number' => $model->number,
            'Date' => $model->date,
        ];
    }

    public function fillFields($attributes)
    {
        $this->I->fillField('Name', $attributes['name']);
        $this->I->fillField('Number', $attributes['number']);
        $this->I->fillField('Date', $attributes['date']);

    }
}
