<?php
namespace app\tests\functional;

use MyModelHelper;

use app\tests\fixtures\UserFixture;
use app\tests\fixtures\MyModelFixture;

class MyModelCest
{
    private $user;
    private $myModel;
    private $myModelHelper;

    public function _before(\FunctionalTester $I)
    {
        $I->haveFixtures([
            'users' => UserFixture::className(),
            'myModels' => MyModelFixture::className(),
        ]);

        $this->user = $I->grabFixture('users', 'user');
        $this->license = $I->grabFixture('myModels', 'myModel');
        $this->myModelHelper = new MyModelHelper($I);
    }

    public function testUserCanManageMyModel(\FunctionalTester $I)
    {
        $I->wantTo('User see my model in root and can go to index');

        $I->checkIndexButtonOnPage($this->user,$this->myModelHelper);
    }

    public function testUserCantManageMyModel(\FunctionalTester $I)
    {
        $I->wantTo('User don\'t see my model in root and can\'t manage them');

        $I->checkIndexButtonOnPageNOTFOUND($this->user,$this->myModelHelper);
    }

    public function testAdminCanDeleteUpdateViewAndCreateMyModel(\FunctionalTester $I)
    {
        $I->wantTo('See if an admin can delete, update, view and create my model');

        $I->checkCommonButtons($this->user,$this->myModelHelper,$this->myModel);
    }

    public function testSeeMyModelInformation(\FunctionalTester $I)
    {
        $I->wantTo('See my model information in index and model information page');

        $I->checkModelInformation($this->user,$this->myModelHelper,$this->myModel);
    }

    public function testCreateMyModel(\FunctionalTester $I)
    {
        $I->wantTo('Create my model');
        $myModel = [
            'Name' => 'MyModel',
            'Number' => 12345,
            'Date' => 03/11/99,
        ];
        $I->checkCreateModel($this->user,$this->myModelHelper,$myModel);
    }

    public function testUpdateMyModel(\FunctionalTester $I)
    {
        $I->wantTo('Update my model');
        $myModelUpdated = [
            'Name' => 'MyModel',
            'Number' => 12345,
            'Date' => 03/11/99,
        ];
        $I->checkUpdateModel($this->user,$this->myModel,$this->myModelHelper,$myModelUpdated);
    }

    public function testDeleteMyModel(\FunctionalTester $I)
    {
        $I->wantTo('Delete my model');

        $I->checkDeleteModel($this->user,$this->myModelHelper,$this->myModel);
    }
}
?>
